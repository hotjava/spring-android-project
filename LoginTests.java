package com.example.appfinancialplanning.presenter;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.example.appfinancialplanning.model.DataModel;
import com.example.appfinancialplanning.model.DataModelInMemory;
import com.example.appfinancialplanning.model.User;
import com.example.appfinancialplanning.presenter.*;

public class LoginTests {
	
	DataModel model;
	
	@Before
	public void setup() {
		model = new DataModelInMemory();
		model.addUser(new User("admin", "pass123"));
	}

	@Test
	//ensures login works with default user and that active user is changed upon successful login
	public void testDefaultLogin() {
		assertEquals(LoginManager.tryLogin("admin", "pass123", model), 0);
		assertTrue(AccountManager.current.equals(model.findUser("admin")));
	}
	
	@Test
	//ensures proper msg is returned if user does not exist
	public void testDoesNotExist() {
		assertEquals(LoginManager.tryLogin("Username", "Not Found", model), 1);
	}
	
	@Test
	//tests that proper msg is returned if username exists but password incorrect
	public void testWrongPassword() {
		assertEquals(LoginManager.tryLogin("admin", "pass12", model), 2);
	}
	
	@Test
	//tests that null doesn't cause any unforseen issues
	public void testNull() {
		assertEquals(LoginManager.tryLogin(null, null, model), 1);
		assertEquals(LoginManager.tryLogin("admin", null, model), 2);
		assertEquals(LoginManager.tryLogin(null, "pass123", model), 1);
		assertEquals(LoginManager.tryLogin(null, null, null), 3);
	}
	
	@Test
	//adds more users to ensure model in memory properly retrieives users
	public void testAddThenLogin() {
		model.addUser(new User("kevin", "2541"));
		model.addUser(new User("bob", "1234"));
		assertEquals(LoginManager.tryLogin("kevin", "1234", model), 2);
		assertEquals(LoginManager.tryLogin("bob", "1234", model), 0);
	}
	
	@Test
	//tests clear on model for functionality purposes
	public void testClear() {
		model.clear();
		assertEquals(LoginManager.tryLogin("admin", "pass123", model), 1);
	}

}
