/**
 * This class defines a user that will be stored for login purposes.
 *
 * @author Kevin Simon
 */

public class User {
	private String name;
	private String password;

	public User(String name, String password) {
		this.name = name;
		this.password = password;
	}

	/**
	 * Two users are equal if their usernames are equal to prevent duplicate usernames
	 */
	public boolean equals(Object o) {
		if(o instanceof User) {
			return name.equals(((User)o).getName());
		}
		return false;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}
}