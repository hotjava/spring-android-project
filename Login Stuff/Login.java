import java.util.Map;

public class Login {

	/**
	 * Checks if user name exists in backing map, if it does not
	 * login immediately fails. If it does exist, check passwords,
	 * if they match login is sucessful, else login fails.
	 * 
	 * @param user User trying to login
	 * @param users map of login names/actual user objects currently in the system
	 */
	public static boolean tryLogin(User user, Map<String,User> users) {
		if(users.containsKey(user.getName())) {
			String password = users.get(user.getName()).getPassword();
			if(user.getPassword().equals(password)) {
				return true;
			}
		}
		return false;
	}
	
}
