/*
created by Caven in March 2014
3 SQLite Tables

Bug: runs correct on Genymotion
	on Galaxy S4 there is a bug: only one account is shown. Maybe related to SQLite Version?

an old database named finance_warehouse was left...

Literally the commented code should work but in fact not! 
When creating 3 tables together, only one is found!

when a user is registered, it is first checked in the database:
	if it exists: can't create
	else: it is registered and loaded into memory
--see RegisterActivity

when the program is started, it will try to add two default users in the database
	demo and admin, see MainActivity

when a user logs in, his/her accounts are loaded into memory;	

when an account is created, it is automatically loaded into memory, together with the transactions associated

note that the user DB has guard against recreation of user ( by setting the primary key),
but the account and transaction DB haven't; it is guarded by loading from DB to memory and the memory guard 
against. This is easy to implement and is able to utilize the code we have created.

 */
package com.example.appfinancialplanning.database;

import java.util.ArrayList;

import com.example.appfinancialplanning.model.Account;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * the class to manage the table of user.
 * @author Caven
 * 
 */
public class DB_Helper extends SQLiteOpenHelper {
	/**
	 * name of the database.
	 */
    private static final String DATABASE_NAME = "user_warehouse";
    /**
     * the database version.
     */
    private static final int DATABASE_VERSION = 1;

    /**
     * name of the table.
     */
    private static final String TABLE_USER = "users";
    // private static final String TABLE_ACCOUNT="accounts";
    // private static final String TABLE_TRANSACTION="transactions";

    // common column names
    /**
     * KEY: username as TEXT.
     */
    private static final String KEY_USER_NAME = "username";
    /**
     * KEY: accountName as TEXT.
     */
    private static final String KEY_ACCOUNT_NAME = "accountName";

    /**
     * KEY: the password as TEXT.
     */
    private static final String KEY_USER_PASSWORD = "password";

    /**
     * 
     */
    private static final String CREATE_TABLE_USER = "CREATE TABLE "
            + TABLE_USER + "(" + KEY_USER_NAME + " TEXT PRIMARY KEY,"
            + KEY_USER_PASSWORD + " TEXT" + ")";

    /*
     * private static final String CREATE_TABLE_ACCOUNT="CREATE TABLE " +
     * TABLE_ACCOUNT + "(" + KEY_USER_NAME + " TEXT PRIMARY KEY," +
     * KEY_ACCOUNT_NAME + " TEXT," + KEY_ACCOUNT_DISPLAY + " TEXT," +
     * KEY_ACCOUNT_RATE + " REAL," + KEY_ACCOUNT_BALANCE + " REAL)";
     * 
     * private static final String CREATE_TABLE_TRANSACTION="CREATE TABLE " +
     * TABLE_TRANSACTION + "(" + KEY_USER_NAME + " TEXT PRIMARY KEY," +
     * KEY_ACCOUNT_NAME + " TEXT PRIMARY KEY," + KEY_TRANS_DATE + " TEXT," +
     * KEY_TRANS_AMOUNT + " REAL," + KEY_TRANS_NOTE + " TEXT," +
     * KEY_TRANS_MERCHANT + " TEXT," + KEY_TRANS_TYPE + " TEXT," +
     * KEY_TRANS_CATEGORY + " TEXT)";
     * 
     * 
     * private static final String CREATE_TABLE_ACCOUNT="CREATE TABLE " +
     * TABLE_ACCOUNT + "(" + KEY_ACCOUNT_DISPLAY + " TEXT," + KEY_ACCOUNT_RATE +
     * " REAL," + KEY_ACCOUNT_BALANCE + " REAL,PRIMARY KEY(" + KEY_USER_NAME +
     * " TEXT," + KEY_ACCOUNT_NAME + " TEXT" + "))";
     * 
     * //note: date is the String so that the constructor is good w/o modifying
     * private static final String CREATE_TABLE_TRANSACTION="CREATE TABLE " +
     * TABLE_TRANSACTION + "(" + KEY_TRANS_DATE + " TEXT," + KEY_TRANS_AMOUNT +
     * " REAL," + KEY_TRANS_NOTE + " TEXT," + KEY_TRANS_MERCHANT + " TEXT," +
     * KEY_TRANS_TYPE + " TEXT," + KEY_TRANS_CATEGORY + " TEXT," +
     * "PRIMARY KEY(" + KEY_USER_NAME + " TEXT," + KEY_ACCOUNT_NAME + " TEXT" +
     * "))";
     */
    public DB_Helper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.d("exam create user table", CREATE_TABLE_USER);
        // Log.d("exam create account table",CREATE_TABLE_ACCOUNT);
        // Log.d("exam create transaction table",CREATE_TABLE_TRANSACTION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // db.execSQL(CREATE_TABLE_ACCOUNT);
        db.execSQL(CREATE_TABLE_USER);
        // db.execSQL(CREATE_TABLE_TRANSACTION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        // db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACCOUNT);
        // db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRANSACTION);
        onCreate(db);
    }

    public boolean userExists(String name) {
        return !getPassword(name).equals("");
    }

    // create a user entry in table users if it doesn't exist
    public long addUser(String name, String pw) {
        if (userExists(name)) { // user already exists
            return -1;
        }

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_USER_NAME, name);
        values.put(KEY_USER_PASSWORD, pw);
        long id = db.insert(TABLE_USER, null, values);
        return id;
    }

    // select * from users where username = 'name'
    // return the password
    public String getPassword(String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_USER + " WHERE "
                + KEY_USER_NAME + " = " + "'" + name + "'";

        Cursor cursor = db.rawQuery(query, null);

        int index = cursor.getColumnIndex(KEY_USER_PASSWORD);
        Log.d("db", "before move-- " + cursor);
        cursor.moveToFirst();

        Log.d("db", "after move-- " + cursor);
        index = cursor.getColumnIndex(KEY_USER_PASSWORD);

        Log.d("db", "index is " + index);
        String pw = "";
        try {
            pw = cursor.getString(index);
        } catch (Exception e) {
            Log.d("db", "getString error");
        }

        return pw;

    }

    /*
     * public long addAccount(String userName,String accountName, String
     * display, double balance, double rate){ SQLiteDatabase
     * db=this.getWritableDatabase(); ContentValues val=new ContentValues();
     * val.put(KEY_USER_NAME, userName); val.put(KEY_ACCOUNT_NAME, accountName);
     * val.put(KEY_ACCOUNT_DISPLAY, display); val.put(KEY_ACCOUNT_BALANCE,
     * balance); val.put(KEY_ACCOUNT_RATE, rate); Log.d("db","record " +
     * userName + ":" + accountName); long id=db.insert(TABLE_ACCOUNT, null,
     * val); return id; }
     */

    // the returned list of accounts has the associated transactions with them!
    // SELECT * FROM TABLE_ACCOUNT WHERE KEY_USER_NAME='user'
    /*
     * public ArrayList<Account> getAccounts(String user){ ArrayList<Account>
     * accounts=new ArrayList<Account>(); String query = "SELECT * FROM " +
     * TABLE_ACCOUNT + " WHERE " + KEY_USER_NAME + " = " + "'" + user + "'";
     * Log.d("db","query is " + query); SQLiteDatabase
     * db=this.getReadableDatabase(); Cursor cursor=null; try{
     * cursor=db.rawQuery(query, null); }catch(Exception e){
     * Log.d("db","query error");; } Log.d("db","queried"); if(cursor==null)
     * return accounts;
     * 
     * if(cursor.moveToFirst()){ do{ String accountName=cursor.getString(1);
     * String display=cursor.getString(2); double rate=cursor.getDouble(3);
     * double balance=cursor.getDouble(4);
     * 
     * Account account=new Account(accountName,display,balance,rate);
     * accounts.add(account);
     * 
     * }while(cursor.moveToNext()); }
     * 
     * 
     * return accounts; }
     */

    // SELECT * FROM users
    public ArrayList<String> getAllUser() {
        ArrayList<String> users = new ArrayList<String>();
        String query = "SELECT * FROM " + TABLE_USER;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                String username = cursor.getString(0);
                // Adding contact to list
                users.add(username);
            } while (cursor.moveToNext());
        }

        return users;
    }

    // DELETE FROM users
    // CAUTION: test purpose only!
    public boolean clearUserTable() {
        String query = "DELETE * FROM " + TABLE_USER;
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_USER, null, null) > 0;

    }

}