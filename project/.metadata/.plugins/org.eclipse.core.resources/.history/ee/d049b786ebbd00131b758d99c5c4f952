package com.example.appfinancialplanning.database;

import java.util.ArrayList;

import com.example.appfinancialplanning.model.Account;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
/**
 * Database helper to open the account table.
 * Note there is no primary key in the table, multiple insertion
 * is prohibited in the model in memory.
 * @author Hot Java
 *
 */
public class DBHelperAccount extends SQLiteOpenHelper {
	/**
	 * The name of the database.
	 */
    private static final String DATABASE_NAME = "account_warehouse";
    
    /**
     * the version of the database.
     */
    private static final int DATABASE_VERSION = 1;

    // the database names
    /**
     * the name of the table.
     */
    private static final String TABLE_ACCOUNT = "accounts";

    // common column names
    /**
     * KEY: name of the user.
     */
    private static final String KEY_USER_NAME = "username";
    /**
     * KEY: name of the account.
     */
    private static final String KEY_ACCOUNT_NAME = "accountName";


    // account table:
    /**
     * KEY: display name.
     */
    private static final String KEY_ACCOUNT_DISPLAY = "display";
    /**
     * KEY: rate of the interest.
     */
    private static final String KEY_ACCOUNT_RATE = "rate";
    /**
     * key: the balance.
     */
    private static final String KEY_ACCOUNT_BALANCE = "balance";

    /**
     * the SQL command to create the table.
     */
    private static final String CREATE_TABLE_ACCOUNT = "CREATE TABLE "
            + TABLE_ACCOUNT + "(" + KEY_USER_NAME + " TEXT," + KEY_ACCOUNT_NAME
            + " TEXT," + KEY_ACCOUNT_DISPLAY + " TEXT," + KEY_ACCOUNT_RATE
            + " REAL," + KEY_ACCOUNT_BALANCE + " REAL)";

    // + "PRIMARY KEY(" + KEY_USER_NAME + "," + KEY_ACCOUNT_NAME + "))";
    /**
     * the constructor.
     * @param context can be an activity.
     */
    public DBHelperAccount(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.d("exam create account table", CREATE_TABLE_ACCOUNT);
    }

    /**
     * create the table when an object is created.
     * @param db the database itself.
     */
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_ACCOUNT);
    }
    /**
     * says what happens on upgrade.
     * @param: db database itself
     */
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACCOUNT);
        onCreate(db);
    }

    public long addAccount(String userName, String accountName, String display,
            double balance, double rate) {
        ArrayList<Account> accounts = getAccounts(userName);

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();
        val.put(KEY_USER_NAME, userName);
        val.put(KEY_ACCOUNT_NAME, accountName);
        val.put(KEY_ACCOUNT_DISPLAY, display);
        val.put(KEY_ACCOUNT_BALANCE, balance);
        val.put(KEY_ACCOUNT_RATE, rate);
        Log.d("db", "record " + userName + ":" + accountName);
        long id = db.insert(TABLE_ACCOUNT, null, val);
        return id;
    }

    // the returned list of accounts has the associated transactions with them!
    // SELECT * FROM TABLE_ACCOUNT WHERE KEY_USER_NAME='user'
    public ArrayList<Account> getAccounts(String user) {
        ArrayList<Account> accounts = new ArrayList<Account>();
        String query = "SELECT * FROM " + TABLE_ACCOUNT + " WHERE "
                + KEY_USER_NAME + " = " + "'" + user + "'";
        Log.d("db", "query is " + query);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(query, null);
        } catch (Exception e) {
            Log.d("db", "query error");
            ;
        }
        Log.d("db", "queried");
        if (cursor == null)
            return accounts;

        if (cursor.moveToFirst()) {
            do {
                String accountName = cursor.getString(1);
                String display = cursor.getString(2);
                double rate = cursor.getDouble(3);
                double balance = cursor.getDouble(4);
                Log.d("db", accountName + " " + display + " " + rate + " "
                        + balance + " ");
                Account account = new Account(accountName, display, balance,
                        rate);
                accounts.add(account);

            } while (cursor.moveToNext());
        }

        return accounts;
    }
    
    public boolean clearTable() {
        String query = "DELETE * FROM " + TABLE_ACCOUNT;
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_ACCOUNT, null, null) > 0;

    }

    public void updateBalance(String username, String accname, double change) {
        String queryPrev = "SELECT * FROM " + TABLE_ACCOUNT + " WHERE "
                + KEY_USER_NAME + " = " + "'" + username + "' AND "
                + KEY_ACCOUNT_NAME + " ='" + accname + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(queryPrev, null);
        int index = cursor.getColumnIndex(KEY_ACCOUNT_BALANCE);
        cursor.moveToFirst();
        double prev = 0;
        try {
            prev = cursor.getDouble(index);
        } catch (IllegalArgumentException e) {
        }

        double now = prev + change;

        String queryUpdate = "UPDATE " + TABLE_ACCOUNT + " SET "
                + KEY_ACCOUNT_BALANCE + " =" + now + "" + " WHERE "
                + KEY_USER_NAME + " = '" + username + "' AND "
                + KEY_ACCOUNT_NAME + " ='" + accname + "'";
        db.execSQL(queryUpdate);
    }

}
