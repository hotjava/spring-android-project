package com.example.appfinancialplanning.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;



/**
 * Represents a user account that will be used for login purposes.
 * 
 * @author Kevin modified by Caven on 2/27: add getAccountNames()
 * 
 */
public class User {
    private String username;
    private String password;
    private Map<String, Account> accounts;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        accounts = new HashMap<String, Account>();
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Account getAccount(String aName) {
        Account account = accounts.get(aName);
        return account;
    }

    public Collection<Account> getAccounts() {
        return accounts.values();
    }

    /**
     * Adds the account to the hashmap of accounts stored in the user.
     * 
     * @param toAdd account to be added
     */
    public void addAccount(Account toAdd) {
        if (toAdd == null) {
            return;
        } else {
            accounts.put(toAdd.getAccountName(), toAdd);
        }
    }

    /**
     * 
     * @return number of accounts in backing data structure.
     */
    public int getAccountNumber() {
        return accounts.size();
    }

    /**
     * @return list of all acccount names.
     */
    public String[] getAccountNames() {
        int num = getAccountNumber();
        String[] acc = new String[num];

        Iterator<String> itr = accounts.keySet().iterator();
        int i = 0;
        while (itr.hasNext()) {
            acc[i++] = (String) itr.next();
        }
        return acc;
    }
}
