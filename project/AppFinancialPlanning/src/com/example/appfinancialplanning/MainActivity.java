package com.example.appfinancialplanning;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.TextView;

import com.example.appfinancialplanning.database.DBHelper;
import com.example.appfinancialplanning.model.DataModel;
import com.example.appfinancialplanning.model.DataModelInMemory;

//import com.example.appfinancialplanning.views.EnterUserView;

/**
 * Main activity representing login screen. When a button is clicked activity is
 * changed to another activity related to the name of the button pressed.
 */

public class MainActivity extends Activity {
    /**
     * accountManager used to handle accounts across app.
     */
    public static DataModel accountManager = new DataModelInMemory();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Typeface tf = Typeface.createFromAsset(getAssets(),
        // "fonts/Satisfy-Regular.ttf");
        // TextView tv = (TextView) findViewById(R.id.creditsTextView);
        // tv.setTypeface(tf);

        // animation for the login button
        Animation translateTitle = new TranslateAnimation(-500, 0, 0, 0);
        translateTitle.setInterpolator(new AccelerateInterpolator()); // add
                                                                      // this
        translateTitle.setDuration(1000);

        // animation for the register button
        Animation translateCredits = new TranslateAnimation(-500, 0, 0, 0);
        translateCredits.setInterpolator(new AccelerateInterpolator()); // add
                                                                        // this
        translateCredits.setDuration(1500);
        // translateRegister.setStartTime(500);

        Animation translateLogin = new TranslateAnimation(-500, 0, 0, 0);
        translateLogin.setInterpolator(new AccelerateInterpolator()); // add
                                                                      // this
        translateLogin.setDuration(2000);

        Animation translateRegister = new TranslateAnimation(-500, 0, 0, 0);
        translateRegister.setInterpolator(new AccelerateInterpolator()); // add
                                                                         // this
        translateRegister.setDuration(2500);

        Button registerButton = (Button) findViewById(R.id.register_button);
        Button loginButton = (Button) findViewById(R.id.login_button);
        TextView appTitleText = (TextView) findViewById(R.id.nameOfAppTextView);
        TextView creditsText = (TextView) findViewById(R.id.creditsTextView);

        AnimationSet animation = new AnimationSet(false); // change to false
        animation.addAnimation(translateTitle);
        appTitleText.startAnimation(translateTitle);
        animation.addAnimation(translateCredits);
        creditsText.startAnimation(translateCredits);
        animation.addAnimation(translateLogin);
        loginButton.startAnimation(translateLogin);
        animation.addAnimation(translateRegister);
        registerButton.startAnimation(translateRegister);

        DBHelper db = new DBHelper(this);
        db.addUser("demo", "pass");
        db.addUser("admin", "pass123");
        // DB_Helper_Account accountDB = new DB_Helper_Account(this);
        // accountDB.addAccount("demo", "google", "google", 1000, 0);
        // accountDB.addAccount("demo", "amazon", "amazon", 2000, 1);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /** Called when the user clicks the login button.
     * @param view view 
     */
    public void loginScreen(View view) {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        MainActivity.this.startActivity(intent);
    }

    /** Called when the user clicks the register button.
     * @param view view 
     */
    public void registerScreen(View view) {
        Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
        MainActivity.this.startActivity(intent);
    }

}
