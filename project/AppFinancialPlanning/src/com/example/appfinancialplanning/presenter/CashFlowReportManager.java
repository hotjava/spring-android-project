package com.example.appfinancialplanning.presenter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

import android.util.Log;

import com.example.appfinancialplanning.AccountDisplayActivity;
import com.example.appfinancialplanning.CashFlowReportActivity;
import com.example.appfinancialplanning.IncomeSourceReportActivity;
import com.example.appfinancialplanning.model.Account;
import com.example.appfinancialplanning.model.Transaction;
import com.example.appfinancialplanning.model.User;

public class CashFlowReportManager {
    //Austin 3/19
	private final CashFlowReportActivity myAct;
	private final User user;
	
	public CashFlowReportManager(CashFlowReportActivity act){
		myAct=act;
		user=LoginManager.selectedUser;
	}
    public String createCashFlowReport(String startString, String endString) {
    	
		SimpleDateFormat dateF= new SimpleDateFormat("MMddyyyy",Locale.US);
		Date start ;
		Date end;
		try{
			 start = dateF.parse(startString);
			 end = dateF.parse(endString);
			 	ArrayList<String> list = new ArrayList<String>();
				Collection<Account> acclist = user.getAccounts();
				double[] catAmount = {0,0};
				for (Account acc:acclist) {
					double[] tempAmount = acc.cashFlowReport(start, end);
					for (int i = 0; i < tempAmount.length; i++) {
						catAmount[i] = catAmount[i] + tempAmount[i];
					}
				}
				list.add("\n");
				list.add("Income "+ "   " + String.valueOf(catAmount[0]) + "\n");
				list.add("Expenses "+ "   " + String.valueOf(catAmount[1]*-1) + "\n");
				double total = catAmount[0] + catAmount[1];
				list.add("Total "+ "   " + String.valueOf(total) + "\n");

				//return list.toString();
				StringBuffer buf=new StringBuffer();
				for(int i=0; i<list.size(); i++){
					buf.append("\t"+list.get(i));
				}
				return buf.toString();
				
		}catch(Exception e){
			Log.d("dateString convert to date", "Constructor Error!");
			return null;
		}
			

	}
}
