package com.example.appfinancialplanning.presenter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import android.util.Log;

import com.example.appfinancialplanning.AccountDisplayActivity;
import com.example.appfinancialplanning.IncomeSourceReportActivity;
import com.example.appfinancialplanning.model.Account;
import com.example.appfinancialplanning.model.Transaction;
import com.example.appfinancialplanning.model.User;

public class IncomeSourceReportManager {
    //Austin 3/19
	private final IncomeSourceReportActivity myAct;
	private final User user;
	public String error;
	
	public IncomeSourceReportManager(IncomeSourceReportActivity act){
		myAct=act;
		user=LoginManager.selectedUser;
		error = "default";
	}
    public String createIncomeSourceReport(String startString, String endString) {
    	
		SimpleDateFormat dateF= new SimpleDateFormat("MMddyyyy",Locale.US);
		Date start ;
		Date end;
		double total = 0;
		try{
			 start = dateF.parse(startString);
			 end = dateF.parse(endString);			 
				ArrayList<String> list = new ArrayList<String>();
				Collection<Account> acclist = user.getAccounts();
				error = "through";
				HashMap<String, Double> aList = new HashMap<String, Double>();
				HashMap<String,Double> tempList = new HashMap<String, Double>();
				for (Account acc : acclist) {
					error = "first loop";
					tempList = acc.incomeSourceReport(start, end);
					error = "after report";
					for (Map.Entry<String, Double> entry : tempList.entrySet()) {
						if(aList.containsKey(entry.getKey())){
		  					double value = aList.get(entry.getKey());
		  					value = value + entry.getValue();
		  					aList.put(entry.getKey(), value);
						}
						else{
							aList.put(entry.getKey(), entry.getValue());
						}
					}
				}
				list.add("\n");
				for (Map.Entry<String, Double> entry : aList.entrySet()) {
					error = "second Loop";
					total = entry.getValue() + total;
					list.add( entry.getKey() + "   " + entry.getValue() + "\n");

				}
				list.add("Total: 	" + total);
				
				//return list.toString();
				StringBuffer buf=new StringBuffer();
				for(int i=0; i<list.size(); i++){
					buf.append("\t"+list.get(i));
				}
				return buf.toString();
				
		}catch(Exception e){
			Log.d("dateString convert to date", "Constructor Error!");
			return error;
		}
			

	}
}
