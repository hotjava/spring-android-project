package com.example.appfinancialplanning.presenter;

import com.example.appfinancialplanning.model.Account;
import com.example.appfinancialplanning.model.User;

/**
 * Really Basic Account Manager system, Checks the parameters to see if they
 * work, and If everything verifies than it Calls the Add Account Function for
 * current.
 * 
 * @author Austin Musselwhite
 */
public class AccountManager {
    /**
     * current user.
     */
    public static User current;
    // finish this
    /**
     * constant for invalid full name error.
     */
    public static final int INVALID_FULL_NAME = 5;
    /**
     * constant for invalid display name error.
     */
    public static final int INVALID_DISPLAY_NAME = 4;
    /**
     * constant for negative balance error.
     */
    public static final int NEGATIVE_BALANCE = 2;
    /**
     * constant for invalid negative interest rate error.
     */
    public static final int NEGATIVE_INTEREST_RATE = 3;
    /**
     * constant if no error occurs during account creation.
     */
    public static final int NO_PROBLEM = 0;
    /**
     * constant if account exists error.
     */
    public static final int ACCOUNT_EXISTS = 1;

    /**
     * tries creating an account.
     * @param fullName name of acocunt
     * @param displayName name shown to ui
     * @param balance money in account
     * @param interestRate associated with account
     * @return condition of error
     */
    public static int tryCreateAccount(final String fullName,
            final String displayName, final Double balance,
            final Double interestRate) {
        if (fullName.length() == 0) {
            return INVALID_FULL_NAME;
        } else if (displayName.length() == 0) {
            return INVALID_DISPLAY_NAME;
        } else if (current.getAccount(fullName) != null) {
            return ACCOUNT_EXISTS;
        } else if (balance < 0) {
            return NEGATIVE_BALANCE;
        } else if (interestRate < 0) {
            return NEGATIVE_INTEREST_RATE;
        } else {
            Account newAcc = new Account(fullName, displayName, balance,
                    interestRate);
            current.addAccount(newAcc);
            return NO_PROBLEM;
        }
    }

}
