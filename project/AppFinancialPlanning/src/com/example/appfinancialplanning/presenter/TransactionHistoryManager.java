package com.example.appfinancialplanning.presenter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import android.util.Log;

import com.example.appfinancialplanning.SpendingCategoryReportActivity;
import com.example.appfinancialplanning.TransactionHistoryActivity;
import com.example.appfinancialplanning.model.Account;
import com.example.appfinancialplanning.model.Transaction;
import com.example.appfinancialplanning.model.User;
/**
 * Handles logic for spending category report.
 * @author Hot Java
 *
 */
public class TransactionHistoryManager {
    // Chao 3/19

	/**
	 * user to get report for.
	 */
    private final User user;
    private Account acc;

    /**
     * sets user.
     * @param act activity to display information on
     */
    public TransactionHistoryManager(final TransactionHistoryActivity act, Account acc) {
        user = LoginManager.selectedUser;
        this.acc = acc;
    }

    /**
     * generate report.
     * @param startString date at which report begins
     * @param endString date at which report ends
     * @return final report.
     */
    public final String createTransactionHistory(final String startString,
            final String endString) {

        SimpleDateFormat dateF = new SimpleDateFormat("MMddyyyy", Locale.US);
        Date start;
        Date end;
        try {
            start = dateF.parse(startString);
            end = dateF.parse(endString);

            ArrayList<String> list = new ArrayList<String>();
            ArrayList<Transaction> aList = new ArrayList<Transaction>();
            aList = acc.getTransactionList();
            // "entertainment"
            list.add("\n");
            list.add("Committed:");
            list.add("\n");
            Collections.sort(aList, new Comparator<Transaction>(){
            	public int compare(Transaction t1, Transaction t2){
            		return t1.getDate().compareTo(t2.getDate());
            	}
            });
            for (Transaction trans : aList) {
                if (trans.getDate().after(start)
                            && trans.getDate().before(end)) {
                	String type = "withdrawal";
                	if(trans.getAmount() > 0){
                		type = "deposit";
                	}
                	SimpleDateFormat dateZ = new SimpleDateFormat("MMM dd yyyy", Locale.US);
                	String aDate = dateZ.format(trans.getDate());
                	list.add(aDate + "   " + type + "   " + trans.getCategory()+ "   "
                             + String.valueOf(trans.getAmount()) + "\n");
                }
            }
            // return list.toString();
            StringBuffer buf = new StringBuffer();
            for (int i = 0; i < list.size(); i++) {
                buf.append("\t" + list.get(i));
            }
            return buf.toString();

        } catch (ParseException e) {
            Log.d("dateString convert to date", "Constructor Error!");
            return null;
        }

    }
}
