package com.example.appfinancialplanning.presenter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

import android.util.Log;

import com.example.appfinancialplanning.AccountListingReportActivity;
import com.example.appfinancialplanning.CashFlowReportActivity;
import com.example.appfinancialplanning.SpendingCategoryReportActivity;
import com.example.appfinancialplanning.model.Account;
import com.example.appfinancialplanning.model.Transaction;
import com.example.appfinancialplanning.model.User;
/**
 * Handles logic for spending category report.
 * @author Hot Java
 *
 */
public class AccountListingReportManager {
    // Chao 3/19

	/**
	 * user to get report for.
	 */
	private final AccountListingReportActivity myAct;

    private final User user;

    /**
     * sets user.
     * @param act activity to display information on
     */
    public AccountListingReportManager(final AccountListingReportActivity act) {
		myAct=act;
    	user = LoginManager.selectedUser;
    }

    /**
     * generate report.
     * @param startString date at which report begins
     * @param endString date at which report ends
     * @return final report.
     */
    public final String createAccountListingReport(final String startString,
            final String endString) {

        SimpleDateFormat dateF = new SimpleDateFormat("MMddyyyy", Locale.US);
        Date start;
        Date end;
        try {
            start = dateF.parse(startString);
            end = dateF.parse(endString);

            ArrayList<String> list = new ArrayList<String>();
            Collection<Account> acclist = user.getAccounts();
            list.add("\n");
            for (Account acc : acclist) {
            	 list.add(acc.getDisplayName() + "   "
                         + String.valueOf(acc.getBalance()) + "\n");
            }
            list.add("\n");
            // return list.toString();
            StringBuffer buf = new StringBuffer();
            for (int i = 0; i < list.size(); i++) {
                buf.append("\t" + list.get(i));
            }
            return buf.toString();

        } catch (ParseException e) {
            Log.d("dateString convert to date", "Constructor Error!");
            return null;
        }

    }
}
