package com.example.appfinancialplanning.presenter;
import com.example.appfinancialplanning.model.Account;
import com.example.appfinancialplanning.model.Transaction;


/**
 * Check parameters, call add functions to history.
 * 
 * @author Chao
 * 
 */
public class TransactionManager {
    /**
     * current account.
     */
    private Account acc;
    // finish this

    /**
     * constant if no errors are found.
     */
    public static final int NO_PROBLEM = 0;
    /**
     * constant for invalid amount error.
     */
    public static final int INVALID_AMOUNT = 1;
    /**
     * constant for invalid date error.
     */
    public static final int INVALID_DATE = 2;

    /**
     * constructor for transaction manager.
     * @param acc account that transaction manager will add transactions to
     */
    public TransactionManager(final Account acc) {
        this.acc = acc;
    }

    /**
     * return account.
     * @return account.
     */
    public final Account getAccount() {
        return acc;
    }

    /**
     * 
     * @param amount transaction amount.
     * @param date transaction occured.
     * @param category transaction category.
     * @param note optional note associated with transaction.
     * @return int depending on where transaction errored.
     */
    public final int tryCreateTransaction(final double amount,
            final String date, final String category, final String note) {
        //Log.d(date, category);
        if (amount == 0) {
            return INVALID_AMOUNT;
        } else if (date.length() <= 0) { // DDMMYYYY
            return INVALID_DATE;
        } else {
            //Log.d("newTra", "begin");
            Transaction newTr = new Transaction(amount, date);
            if (category.length() > 0) {
                newTr.setCategory(category);
            }
            if (note.length() > 0) {
                newTr.setNote(note);
            }
            //Log.d("newTra", "End");
            //Log.d("addTra", String.valueOf(newTr.getAmount()));
            acc.addTransaction(newTr);
            //Log.d("addTra", "End");
            // double b = acc.getBalance();
            //Log.d("Get", "Balance");
            // acc.setBalance(b-amount);
            //Log.d("Set", "Balance");
            return NO_PROBLEM;
        }
    }
}
