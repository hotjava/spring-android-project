/**
 * 
 * created By Caven on 2/27
 * Add getAccount Chao 2/28/2014
 */

package com.example.appfinancialplanning.presenter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import com.example.appfinancialplanning.AccountDisplayActivity;
import com.example.appfinancialplanning.model.Account;
import com.example.appfinancialplanning.model.Transaction;
import com.example.appfinancialplanning.model.User;

/**
 * user manager object.
 * @author Hot JAva
 *
 */
public class UserManager {
    /**
     * current user.
     */
    private final User user;

    /**
     * sets current user.
     * @param act account display activity
     */
    public UserManager(final AccountDisplayActivity act) {
        user = LoginManager.selectedUser;
    }

    /**
     *
     * @return number associated with user's account
     */
    public final int showAccountNumber() {
        return user.getAccountNumber();
    }

    /**
     * 
     * @return userName
     */
    public final String getUserName() {
        return user.getUsername();
    }

    /**
     * 
     * @return all user account names
     */
    public final String[] getAccountNames() {
        return user.getAccountNames();
    }

    // Chao:get account
    /**
     * 
     * @param aName name of account desired
     * @return account based on a name
     */
    public final Account getAccount(final String aName) {
        return user.getAccount(aName);
    }

    // Chao 3/19
    /**
     * 
     * @return all counts
     */
    public final Collection<Account> getAllAccounts() {
        return user.getAccounts();
    }

    /**
     * 
     * @param accounts list of all accounts
     * @param transactions list of all transactions
     */
    public final void loadAccounts(final ArrayList<Account> accounts,
            final HashMap<String, ArrayList<Transaction>> transactions) {
        for (Account acc : accounts) {
            // Log.d("db","for " + acc.getAccountName() + " in DB");
            ArrayList<Transaction> addOn = transactions.get(acc
                    .getAccountName());
            if (addOn != null) {
                acc.setTransactionList(addOn);
            }
            user.addAccount(acc);
        }
    }
}
