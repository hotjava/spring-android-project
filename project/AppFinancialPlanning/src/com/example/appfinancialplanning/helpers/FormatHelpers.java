package com.example.appfinancialplanning.helpers;

import android.annotation.SuppressLint;

import java.util.Locale;
import java.text.ParseException;
import java.text.SimpleDateFormat;
/**
 * helps formating.
 * @author Hot Java
 *
 */
public class FormatHelpers {
    /**
     * checks if date is valid.
     * @param dateStr date in string format.
     * @return if date is valid (true) or not
     */
    @SuppressLint("SimpleDateFormat")
    public static boolean dateIsValid(String dateStr) {

        SimpleDateFormat dateF = new SimpleDateFormat("MMddyyyy", Locale.US);
        boolean noError = true;
        try {
            // dateF.setLenient(false);
            dateF.parse(dateStr);
        } catch (ParseException e) {
            noError = false;
        }

        return noError;
    }
}
