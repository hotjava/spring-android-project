package com.example.appfinancialplanning;

import com.example.appfinancialplanning.helpers.FormatHelpers;
import com.example.appfinancialplanning.presenter.CashFlowReportManager;
import com.example.appfinancialplanning.presenter.IncomeSourceReportManager;
import com.example.appfinancialplanning.presenter.UserManager;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
/**
 * 
 * @author Chao 3/19
 *
 */
public class CashFlowReportActivity extends Activity {

	static private CashFlowReportManager cashManager;
	String report;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cash_flow_report);
		cashManager = new CashFlowReportManager(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.cash_flow_report, menu);
		return true;
	}
	public void onClickShowCashFlowReport(View view) {
		//if(true){
		if(getDateStart().getText().length()==0 && getDateEnd().getText().length()==0){
			report = cashManager.createCashFlowReport("01011999","12312050");
			getHintBar().setText("Cash Flow Report  "  + report); //+"\""
			//getHintBar().setText("start at " + getDateStart().getText().length() 
					//+ " end at " + getDateEnd().getText().length());
			return;
		}
		
    	String startString = getDateStart().getText().toString();    	
    	if(!FormatHelpers.dateIsValid(startString)){
    		getHintBar().setText("Date format " + startString + " is invalid");
    		return;
    	}
    	
    	String endString = getDateEnd().getText().toString();    	
    	if(!FormatHelpers.dateIsValid(endString)){
    		getHintBar().setText("Date format " + endString + " is invalid");
    		return;
    	}
		report = cashManager.createCashFlowReport(startString, endString);
		getHintBar().setText("Cash Flow Report  "  + report); //+"\""
	}
	
    public void onClickGoBack(View view){
    	Intent intent = new Intent(CashFlowReportActivity.this,AccountDisplayActivity.class);//AccountDisplayActivity
    	CashFlowReportActivity.this.startActivity(intent);
    	
    }
	
    private TextView getHintBar(){
    	return (TextView)findViewById(R.id.tra_bar);
    }
    private EditText getDateStart() {
        return (EditText)findViewById(R.id.date_start);
    }
    
    private EditText getDateEnd() {
        return (EditText)findViewById(R.id.date_end);
    }
}
