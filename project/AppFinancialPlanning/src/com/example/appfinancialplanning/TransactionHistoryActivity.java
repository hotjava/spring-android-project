package com.example.appfinancialplanning;

import com.example.appfinancialplanning.helpers.FormatHelpers;
import com.example.appfinancialplanning.model.Account;
import com.example.appfinancialplanning.presenter.SpendingCategoryReportManager;


import com.example.appfinancialplanning.presenter.TransactionHistoryManager;
import com.example.appfinancialplanning.presenter.UserManager;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * 
 * @author Chao 3/19
 * 
 */
public class TransactionHistoryActivity extends Activity {
    
    /**
     * @param spendingCategoryReport
     */
    private static TransactionHistoryManager transHistManager;
    
    /**
     * @param report
     */
    String report;
    private String accName;
    private Account acc;
    private static UserManager userManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history_report);
        Bundle extrasBundle = getIntent().getExtras();
        accName = extrasBundle.getString("AccountName");
        userManager = new UserManager(null); // Initialize!
        acc = userManager.getAccount(accName);
        transHistManager = new TransactionHistoryManager(this, acc);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.transaction_history_report, menu);
        return true;
    }

    /**
     * @param view view
     */
    public void onClickShowTransactionHistory(View view) {
        // if(true){
        if (getDateStart().getText().length() == 0
                && getDateEnd().getText().length() == 0) {
            report = transHistManager.createTransactionHistory("01011999",
                    "12312050");
            getHintBar().setText("Transaction History " + report); // +"\""
            // getHintBar().setText("start at " +
            // getDateStart().getText().length()
            // + " end at " + getDateEnd().getText().length());
            return;
        }

        String startString = getDateStart().getText().toString();
        if (!FormatHelpers.dateIsValid(startString)) {
            getHintBar().setText("Date format " + startString + " is invalid");
            return;
        }

        String endString = getDateEnd().getText().toString();
        if (!FormatHelpers.dateIsValid(endString)) {
            getHintBar().setText("Date format " + endString + " is invalid");
            return;
        }
        report = transHistManager.createTransactionHistory(startString,
                endString);
        getHintBar().setText("Transaction History  " + report); // +"\""
    }

    /**
     * @param view view
     */
    public void onClickGoBack(View view) {
        Intent intent = new Intent(TransactionHistoryActivity.this,
                AccountDisplayActivity.class); // AccountDisplayActivity
        TransactionHistoryActivity.this.startActivity(intent);

    }

    /**
     * @return EditText
     */
    private TextView getHintBar() {
        return (TextView) findViewById(R.id.tra_bar);
    }

    /**
     * @return EditText
     */
    private EditText getDateStart() {
        return (EditText) findViewById(R.id.date_start);
    }
    
    /**
     * @return EditText
     */
    private EditText getDateEnd() {
        return (EditText) findViewById(R.id.date_end);
    }
}
