package com.example.appfinancialplanning;

import com.example.appfinancialplanning.helpers.FormatHelpers;
import com.example.appfinancialplanning.presenter.IncomeSourceReportManager;
import com.example.appfinancialplanning.presenter.UserManager;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
/**
 * 
 * @author Chao 3/19
 *
 */
public class IncomeSourceReportActivity extends Activity {

	static private IncomeSourceReportManager incomeManager;
	String report;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_income_source_report);
		incomeManager = new IncomeSourceReportManager(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.income_source_report, menu);
		return true;
	}
	public void onClickShowIncomeSourceReport(View view) {
		//if(true){
		if(getDateStart().getText().length()==0 && getDateEnd().getText().length()==0){
			report = incomeManager.createIncomeSourceReport("01011999","12312050");
			getHintBar().setText("Income Source Report  "  + report); //+"\""
			//getHintBar().setText("start at " + getDateStart().getText().length() 
					//+ " end at " + getDateEnd().getText().length());
			return;
		}
		
    	String startString = getDateStart().getText().toString();    	
    	if(!FormatHelpers.dateIsValid(startString)){
    		getHintBar().setText("Date format " + startString + " is invalid");
    		return;
    	}
    	
    	String endString = getDateEnd().getText().toString();    	
    	if(!FormatHelpers.dateIsValid(endString)){
    		getHintBar().setText("Date format " + endString + " is invalid");
    		return;
    	}
		report = incomeManager.createIncomeSourceReport(startString, endString);
		getHintBar().setText("Income Source Report  "  + report); //+"\""
	}
	
    public void onClickGoBack(View view){
    	Intent intent = new Intent(IncomeSourceReportActivity.this,AccountDisplayActivity.class);//AccountDisplayActivity
    	IncomeSourceReportActivity.this.startActivity(intent);
    	
    }
	
    private TextView getHintBar(){
    	return (TextView)findViewById(R.id.tra_bar);
    }
    private EditText getDateStart() {
        return (EditText)findViewById(R.id.date_start);
    }
    
    private EditText getDateEnd() {
        return (EditText)findViewById(R.id.date_end);
    }
}
