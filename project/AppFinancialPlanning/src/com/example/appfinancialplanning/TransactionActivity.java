package com.example.appfinancialplanning;

import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.appfinancialplanning.database.DBHelperAccount;
import com.example.appfinancialplanning.database.DBHelperTrans;
import com.example.appfinancialplanning.helpers.FormatHelpers;
import com.example.appfinancialplanning.model.Account;
import com.example.appfinancialplanning.model.Transaction;
import com.example.appfinancialplanning.presenter.TransactionManager;
import com.example.appfinancialplanning.presenter.UserManager;

/**
 * Activity that handles setting up transaction UI.
 * @author simon3
 *
 */
public class TransactionActivity extends Activity {
    /**
     * manages all users across app.
     */
    private static UserManager userManager;
    /**
     * account name.
     */
    private String accName;
    /**
     * contains logic for transaction.
     */
    private TransactionManager traManager;
    /**
     * account.
     */
    private Account acc;
    // public static final String[] VALID_CATEGORY = {"food", "rent", "auto",
    // "clothing", "entertainment"};
    /**
     * category list.
     */
    private List<String> catList = Arrays.asList(Transaction.VALID_CATEGORY);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);

        Bundle extrasBundle = getIntent().getExtras();
        accName = extrasBundle.getString("AccountName");

        userManager = new UserManager(null); // Initialize!
        acc = userManager.getAccount(accName);
        traManager = new TransactionManager(acc);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.transaction, menu);
        return true;
    }

    /**
     * radio buttons for category.
     * @return radiogroup
     */
    private RadioGroup getRadioCategory() {
        return (RadioGroup) findViewById(R.id.radioCategory);
    }

    /**
     * if radio button is clicked.
     * @param src src
     */
    public void onRadioClicked(View src) {
        int selected = getRadioCategory().getCheckedRadioButtonId();
        RadioButton chosen = (RadioButton) findViewById(selected);
        getCategory().setText(chosen.getText());
    }

    /**
     * if food radio button is clicked.
     * @param src src
     */
    public void onFoodSelected(View src) {
        getCategory().setText("food");
    }
    /**
     * if auto radio button is clicked.
     * @param src src
     */
    public void onAutoSelected(View src) {
        getCategory().setText("auto");
    }
    /**
     * if rent radio button is clicked.
     * @param src src
     */
    public void onRentSelected(View src) {
        getCategory().setText("rent");
    }
    /**
     * if clothing radio button is clicked.
     * @param src src
     */
    public void onClothingSelected(View src) {
        getCategory().setText("clothing");
    }
    /**
     * if entertainment radio button is clicked.
     * @param src src
     */
    public void onEntertainSelected(View src) {
        getCategory().setText("entertainment");
    }

    /**
     * handles ui change when you click add transaction.
     * @param view view
     */
    public void onClickAddTransaction(View view) {
        // Double amount = Double.parseDouble(getAmount().getText().toString());
        // Log.d("amount",String.valueOf(amount));
        String amountStr = getAmount().getText().toString();
        Double amount = Double.valueOf(0.0);
        try {
            amount = Double.parseDouble(amountStr);
        } catch (NumberFormatException nfe) {
            getHintBar().setText("Invalid amount format");
            return;
        }

        if (isExpenditure()) {
            double tmp = amount.doubleValue();
            amount = Double.valueOf(-1 * tmp);
        }

        String date = getDate().getText().toString();

        if (!FormatHelpers.dateIsValid(date)) {
            getHintBar().setText("Date format " + date + " is invalid");
            return;
        }

        String category = getCategory().getText().toString();
        if (category.length() == 0) {
            getHintBar()
                    .setText(
                            "invalid category, choose from: food, rent, auto, clothing, entertainment");
            return;
        }

        String note = getNote().getText().toString();

        // Log.d(accName,"start");
        // Account acc = userManager.getAccount(accName);
        // Log.d("Add",acc.getAccountName());
        // Log.d("Add",String.valueOf(acc.getBalance()));
        int result = traManager.tryCreateTransaction(amount, date, category,
                note);
        // Log.d("ENd","result");
        if (result == 0) {
            DBHelperTrans db = new DBHelperTrans(this);
            db.addTransaction(AccountDisplayActivity.username, accName, date,
                    amount, note, "", "", category);
            Log.d("db", "add transaction to db");
            // Creation Successful, Account Created
            // Intent intent = new Intent(AccountActivity.this, .class);
            // AccountActivity.this.startActivity(intent);
            TextView v = (TextView) findViewById(R.id.tra_result);
            v.setText("Add Successful");
            // Note: update database, too!
            DBHelperAccount dbAcc = new DBHelperAccount(this);
            dbAcc.updateBalance(AccountDisplayActivity.username, accName,
                    amount);

            Log.d("Back", acc.getAccountName());
            // this.finish();
            // TransactionActivity.this.startActivity(intent);
            Intent intent = new Intent(TransactionActivity.this,
                    AccountDetailActivity.class);
            intent.putExtra("AccountInfo", accName);
            TransactionActivity.this.startActivity(intent);
            // backToDisplay();
        } else if (result == 1) {
            // creation unsuccessful, Account already in use
            TextView v = (TextView) findViewById(R.id.tra_result);
            v.setText("Add Failed: Invalid Amount");
        } else if (result == 2) {
            // creation unsuccessful, Balance is negative
            TextView v = (TextView) findViewById(R.id.tra_result);
            v.setText("Add Failed: Invalid Date");

        } else {
            TextView v = (TextView) findViewById(R.id.tra_result);
            v.setText("Creation Failed: Form Invalid");
        }
        // this.finish();
    }

    /**
     * 
     * @return hintbar
     */
    private TextView getHintBar() {
        return (TextView) findViewById(R.id.tra_bar);
    }

    /**
     * 
     * @return if is expenditure.
     */
    public boolean isExpenditure() {
        return getIsExpenditure().isChecked();
    }

    /**
     * 
     * @return return if is expenditure ui
     */
    private CheckBox getIsExpenditure() {
        return (CheckBox) findViewById(R.id.is_spend_checkbox);
    }

    /*
     * private void backToDisplay(){ Intent intent = new
     * Intent(TransactionActivity.this,AccountDetailActivity.class);
     * TransactionActivity.this.startActivity(intent);
     * 
     * }
     */
    /**
     * returns amount.
     * @return edittext
     */
    private EditText getAmount() {
        return (EditText) findViewById(R.id.amount);
    }

    /*
     * private EditText getMerchant() { return
     * (EditText)findViewById(R.id.merchant); }
     */
    /**
     * returns category.
     * @return edittext
     */
    private EditText getCategory() {
        return (EditText) findViewById(R.id.category);
    }
    /**
     * returns date.
     * @return edittext
     */
    private EditText getDate() {
        return (EditText) findViewById(R.id.date);
    }

    /**
     * returns note.
     * @return edittext
     */
    private EditText getNote() {
        return (EditText) findViewById(R.id.note);
    }

}
