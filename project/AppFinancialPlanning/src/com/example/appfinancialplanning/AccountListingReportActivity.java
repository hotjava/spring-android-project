package com.example.appfinancialplanning;

import com.example.appfinancialplanning.helpers.FormatHelpers;
import com.example.appfinancialplanning.presenter.AccountListingReportManager;
import com.example.appfinancialplanning.presenter.SpendingCategoryReportManager;



import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * 
 * @author Chao 3/19
 * 
 */
public class AccountListingReportActivity extends Activity {
    
    /**
     * @param spendingCategoryReport
     */
    private static AccountListingReportManager accPort;
    
    /**
     * @param report
     */
    String report;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_listing_report);
        accPort = new AccountListingReportManager(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.account_listing_report, menu);
        return true;
    }

    /**
     * @param view view
     */
    public void onClickShowAccountListingReport(View view) {
        // if(true){
        if (getDateStart().getText().length() == 0
                && getDateEnd().getText().length() == 0) {
            report = accPort.createAccountListingReport("01011999",
                    "12312050");
            getHintBar().setText("Account Listing Report  " + report); // +"\""
            // getHintBar().setText("start at " +
            // getDateStart().getText().length()
            // + " end at " + getDateEnd().getText().length());
            return;
        }

        String startString = getDateStart().getText().toString();
        if (!FormatHelpers.dateIsValid(startString)) {
            getHintBar().setText("Date format " + startString + " is invalid");
            return;
        }

        String endString = getDateEnd().getText().toString();
        if (!FormatHelpers.dateIsValid(endString)) {
            getHintBar().setText("Date format " + endString + " is invalid");
            return;
        }
        report = accPort.createAccountListingReport(startString,
                endString);
        getHintBar().setText("Account Listing Report:  " + report); // +"\""
    }

    /**
     * @param view view
     */
    
    public void onClickGoBack(View view) {
        Intent intent = new Intent(AccountListingReportActivity.this,
                AccountDisplayActivity.class); // AccountDisplayActivity
        AccountListingReportActivity.this.startActivity(intent);

    }

    /**
     * @return EditText
     */
    private TextView getHintBar() {
        return (TextView) findViewById(R.id.tra_bar);
    }

    /**
     * @return EditText
     */
    private EditText getDateStart() {
        return (EditText) findViewById(R.id.date_start);
    }
    
    /**
     * @return EditText
     */
    private EditText getDateEnd() {
        return (EditText) findViewById(R.id.date_end);
    }
}
