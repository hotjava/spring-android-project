package com.example.appfinancialplanning.model;

/**
 * Interface outlining how a data model for account management works. Pretty
 * simple.
 * 
 * @author Kevin and Caven
 */

public interface DataModel {

    /**
     * Attempts to add user to map, returns true if successful. Success is
     * dependant on username not existing in backing map.
     * 
     * @param user
     *            User that is trying to be added to backing map
     * @return if add was successful or not
     */
    boolean addUser(User user);

    /**
     * Given a username returns a user in the backing map. Null if user doesn't
     * exist.
     * 
     * @param username
     *            user be searched for
     * @return returns user if found, null otherwise
     */
    User findUser(String username);

    /**
     * clear data.
     */
    void clear();
}
