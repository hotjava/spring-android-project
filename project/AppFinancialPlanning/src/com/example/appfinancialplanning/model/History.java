package com.example.appfinancialplanning.model;

//import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * 
 * @author Chao 2/27/2014
 * 
 */
public class History {
    /**
     * list of transactions to easily get history.
     */
    private ArrayList<Transaction> historyList;

    public HashMap<String, Double> incomeList;


    // public static final String[] VALID_CATEGORY = {"food", "rent", "auto",
    // "clothing", "entertainment"};

    // Chao 3/19
    /**
     * 
     * @param start beginning of requested report date
     * @param end end of requested report date
     * @return spending report by category over start and end date
     */
    public double[] spendingCategoryReport(Date start, Date end) {
        double[] catAmount = {0, 0, 0, 0, 0}; // "food", "rent", "auto",
        for (Transaction tr : historyList) {
            if (tr.getAmount() < 0 && tr.getDate().after(start)
                    && tr.getDate().before(end)) {
                for (int i = 0; i < catAmount.length; i++) {
                    if (tr.getCategory().equalsIgnoreCase(
                            Transaction.VALID_CATEGORY[i])) {
                        catAmount[i] = catAmount[i] + tr.getAmount();
                    }
                }
            }
        }
        return catAmount;
    }

    public double[] cashFlowReport(Date start, Date end){
    	double[] flowAmounts = {0,0};
    	for(Transaction tr : historyList){
    		if(tr.getAmount()>0 && tr.getDate().after(start) && tr.getDate().before(end)){
    			flowAmounts[0] = flowAmounts[0] + tr.getAmount();
    		}
    		if(tr.getAmount()<0 && tr.getDate().after(start) && tr.getDate().before(end)){
    			flowAmounts[1] = flowAmounts[1] + tr.getAmount();
    		}
    	}
    	return flowAmounts;
    }

    public HashMap<String, Double> IncomeSourceReport(Date start, Date end){
    	incomeList = new HashMap<String,Double>();
  		for(Transaction tr : historyList){
  			if(tr.getAmount() > 0 && tr.getDate().after(start) && tr.getDate().before(end)){
  				if(incomeList.containsKey(tr.getCategory())){
  					double value = incomeList.get(tr.getCategory());
  					value = value + tr.getAmount();
  					incomeList.put(tr.getCategory(), value);
  				}
  				else{
  					incomeList.put(tr.getCategory(), tr.getAmount());
  				}
  			}
  		}
  		return incomeList;
  	}



    /**
     * history constructor, just sets list instance variable.
     */
    public History() {
        historyList = new ArrayList<Transaction>();
    }

    /**
     * 
     * @return history list
     */
    public ArrayList<Transaction> getHistory() {
        return historyList;
    }

    /**
     * 
     * @param list list to set as history
     */
    public void setHistory(ArrayList<Transaction> list) {
        historyList = list;
    }

    /**
     * 
     * @param tra transaction to be added
     */
    public void addTransaction(Transaction tra) {
        historyList.add(tra);
    }

    /**
     * 
     * @param tra transation to be removed
     */
    public void removeTransaction(Transaction tra) {
        historyList.remove(tra);
    }

}
