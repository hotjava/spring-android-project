package com.example.appfinancialplanning.model;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;


/**
 * Represents the financial account of a user that they can access after logging
 * in. Stores 4 variables that represent the financial account.
 * 
 * @author Austin
 * 
 *         Add transaction history
 * @author Chao 2/27/2014
 */
public class Account {
    /**
     * balance.
     */
    private double balance;
    /**
     * name of account.
     */
    private String accountName;
    /**
     * name displayed to user.
     */
    private String displayName;
    /**
     * interest rate of account.
     */
    private double intRate;
    /**
     * history object that stores users transaction history.
     */
    private History history; // Chao: Add history
    /**
     * account constructor.
     * @param accountName account name
     * @param displayName name displayed on account title
     * @param balance account balance
     * @param intRate interest rate
     */
    public Account(String accountName, String displayName, double balance,
                   double intRate) {
        this.accountName = accountName;
        this.displayName = displayName;
        this.balance = balance;
        this.intRate = intRate;
        this.history = new History();
    }

    /**
     * setter for acocunt.
     * @param name set
     */
    public void setAccountName(String name) {
        accountName = name;
    }

    /**
     * 
     * @return account name
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * 
     * @param name set
     */
    public void setDisplayName(String name) {
        displayName = name;
    }

    /**
     * 
     * @param aNum money being added
     * @return new balance after deposit
     */
    public double deposit(double aNum) {
        balance = balance + aNum;
        return balance;
    }

    /**
     * 
     * @param aNum money being removed
     * @return balance after withdrawl
     */
    public double withdrawal(double aNum) {
        balance = balance - aNum;
        return balance;
    }

    /**
     * 
     * @return display name
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * 
     * @return current balance
     */
    public double getBalance() {
        return balance;
    }

    /**
     * 
     * @param aNum new balance
     */
    public void setBalance(double aNum) {
        balance = aNum;
    }

    /**
     * 
     * @return current interest rate
     */
    public double getIntRate() {
        return intRate;
    }

    /**
     * 
     * @param rate you want to set interest to
     */
    public void setIntRate(double rate) {
        intRate = rate;
    }

    // Chao method about transaction history
    /**
     * 
     * @return list of current transactions
     */
    public ArrayList<Transaction> getTransactionList() {
        return history.getHistory();
    }

    /**
     * 
     * @param traList set new transaction list
     */
    public void setTransactionList(ArrayList<Transaction> traList) {
        history.setHistory(traList);
    }

    /**
     * 
     * @param tra transaction to be added
     */
    public void addTransaction(Transaction tra) {
        //Log.d("add", "history-account");
        history.addTransaction(tra);
        //Log.d("add", "history-tra");
        balance = balance + tra.getAmount();
        //Log.d("change balance", String.valueOf(balance));
    }

    /**
     * 
     * @param tra transaction to be removed
     */
    public void removeTransaction(Transaction tra) {
        history.removeTransaction(tra);
    }

    // Chao 3/19
    /**
     * 
     * @param start date you want to start at
     * @param end date you want report to end at
     * @return report for all transactions between two dates
     */
    public double[] spendingCategoryReport(Date start, Date end) {
        return history.spendingCategoryReport(start, end);
    }


    public HashMap<String, Double> incomeSourceReport(Date start, Date end) {
    	return history.IncomeSourceReport(start, end);
    }
    public double[] cashFlowReport(Date start, Date end){
    	return history.cashFlowReport(start, end);
    }

}