package com.example.appfinancialplanning.model;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

import com.example.appfinancialplanning.model.DataModel;
import com.example.appfinancialplanning.model.DataModelInMemory;
import com.example.appfinancialplanning.model.User;

public class DataModelInMemoryTest {

	DataModel model;
	
	
	@Before
	public void setup() {
		model = new DataModelInMemory();
	}
	
	public void testNull() {
		assertNull(model.findUser("Does not exist"));
	}
	
	public void testBasicAdd() {
		User testUser1 = new User("name","123");
		model.addUser(testUser1);
		assertEquals(model.findUser("name"), testUser1);
	}
	
	public void testDuplicateAdd() {
		User testUser2 = new User("aName", "123");
		User testUser3 = new User("aName", "123");
		model.addUser(testUser2);
		model.addUser(testUser3);
		assertEquals(model.findUser("aName"),testUser2);
	}
	

}
