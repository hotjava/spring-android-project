package com.example.appfinancialplanning.presenter;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Before;

import com.example.appfinancialplanning.helpers.FormatHelpers;
import com.example.appfinancialplanning.model.DataModel;
import com.example.appfinancialplanning.model.DataModelInMemory;
import com.example.appfinancialplanning.presenter.*;
import com.example.appfinancialplanning.model.User;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
/**
 * 
 * @author Chao 3/19
 *
 */
public class RegistrationTests{
	
	DataModel aModel;
	@Before
	public void setup(){
		aModel = new DataModelInMemory();
	}
	
	@Test
	//Tests valid Registration
	public void testRegistration(){
		String aString = "User";
		String aPass = "password";
		assertEquals(RegistrationManager.tryRegister(aString,aPass,aModel), 1);
	}
	@Test
	//Tests invalid passwords
	public void testPass(){
		assertEquals(RegistrationManager.tryRegister("User", "", aModel), 0);
		assertEquals(RegistrationManager.tryRegister("User", "", aModel),0);
	}
	@Test
	//Tests it the user already is registered, should not work
	public void testAlreadyExist(){
		aModel.addUser(new User("user", "pass"));
		assertEquals(RegistrationManager.tryRegister("user", "pass", aModel), 2);
	}
}