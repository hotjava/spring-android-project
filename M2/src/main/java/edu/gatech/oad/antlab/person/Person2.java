package edu.gatech.oad.antlab.person;
import java.util.ArrayList;
import java.util.Random;

/**
 *  A simple class for person 2
 *  returns their name and a
 *  modified string 
 *
 * @author Bob
 * @version 1.1
 */
public class Person2 {
    /** Holds the persons real name */
    private String name;
	 	/**
	 * The constructor, takes in the persons
	 * name
	 * @param pname the person's real name
	 */
	 public Person2(String pname) {
	   name = pname;
	 }
	/**
	 * This method should take the string
	 * input and return its characters in
	 * random order.
	 * given "gtg123b" it should return
	 * something like "g3tb1g2".
	 *
	 * @param input the string to be modified
	 * @return the modified string
	 */
    private String calc(String input) {
	//Person 2 put your implementation here
	ArrayList<Character> map = new ArrayList<Character>();
	for(int i=0; i<input.length(); i++){
	    map.add(new Character(input.charAt(i)));
	}
	
	StringBuffer buf=new StringBuffer();
	while(!map.isEmpty()){
	    Random myRandom=new Random();
	    int rd=myRandom.nextInt(map.size());
	    buf.append(map.get(rd));
	    map.remove(rd);
	}
	
	
	return buf.toString();
    }
    
    

    public static void main(String[] arg){
	System.out.println("this is a test");
	Person2 person=new Person2("Caven");
	System.out.println(person.calc("thisisatest"));
    }


    
    
	/**
	 * Return a string rep of this object
	 * that varies with an input string
	 *
	 * @param input the varying string
	 * @return the string representing the 
	 *         object
	 */
	public String toString(String input) {
	  return name + calc(input);
	}
}
